from django.contrib.sites.models import Site
from django.db.models import Q
from django.http import HttpResponse
from django.utils.decorators import method_decorator
from django.views.generic import ListView
from staff.models import Staffer, Department

# Create your views here.

'''
TO DO:
Remove unnecessary model fields.
Re-order legacy id fields.

DONE:
Created IP-checking view/decorator.
64.112.229.222
http://stackoverflow.com/questions/12383540/authenticate-by-ip-address-in-django
https://docs.djangoproject.com/en/1.5/topics/class-based-views/intro/#s-decorating-the-class
https://github.com/registerguard/tracker/issues/489
'''

AUTHORIZED_IPS = ['64.112.229.222',]

def ip_authorization(view_func):
    def authorize(request, *args, **kwargs):
        request_ip = request.META['REMOTE_ADDR']
        if request_ip in AUTHORIZED_IPS or request.user.is_authenticated():
            return view_func(request, *args, **kwargs)
        else:
            return HttpResponse(status=401)
    return authorize

class StaffListView(ListView):
    
    template_name = 'staff/staff_list.html'
    
    def get_queryset(self):
         # return Staffer.objects.filter(active=True).exclude(extension='').exclude(department__department_title='Production').order_by('department', 'last_name')
         return Staffer.objects.filter(active=True).exclude(department__department_title='Production').order_by('department', 'last_name')
    
    def get_context_data(self, **kwargs):
        context = super(StaffListView, self).get_context_data(**kwargs)
        context.update({
            'current_site': Site.objects.get_current(),
            'departments': Department.objects.exclude(department_title='Production'),
            'last_modified': Staffer.objects.all().order_by('-last_modified')[:1],
            'selecteds': Staffer.objects.filter(supra=True, active=True).order_by('top_section__top_section_group', 'supra_order'),
            'page': {'title': 'Contact us', 'description_short': 'staff directory'},
        })
        return context

class PrivateStaffListView(ListView):
    @method_decorator(ip_authorization)
    def dispatch(self, *args, **kwargs):
        return super(PrivateStaffListView, self).dispatch(*args, **kwargs)

    template_name = 'staff/staff_list_internal.html'

    def get_queryset(self):
        return Staffer.objects.filter(Q(active=True, department__department_title='News') | Q(last_name='Lyon')).order_by('last_name')
