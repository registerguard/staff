from os import path
from django.core import exceptions
from django.db import models
from sorl.thumbnail import get_thumbnail, ImageField

class TopSection(models.Model):
    top_section_group = models.CharField(max_length=150)
    extra_text = models.CharField(max_length=128, blank=True)
    slug = models.SlugField(max_length=120, unique=True, blank=True)
    
    class Meta:
        ordering = ['top_section_group']
    
    def __unicode__(self):
        return u'%s' % (self.top_section_group)

class Department(models.Model):
    '''
    See https://github.com/praekelt/django-category/blob/master/category/models.py
    '''
    department_title = models.CharField(max_length=128)
    slug = models.SlugField(max_length=120, unique=True)
    parent = models.ForeignKey('self', blank=True, null=True, related_name='child')
    
    class Meta:
        ordering = ['department_title']
    
    def __unicode__(self):
        p_list = self._recurse_for_parents(self)
        p_list.append(self.department_title)
        return self.get_separator().join(p_list)
    
    def _recurse_for_parents(self, cat_obj):
        p_list = []
        if cat_obj.parent_id:
            p = cat_obj.parent
            p_list.append(p.department_title)
            more = self._recurse_for_parents(p)
            p_list.extend(more)
        if cat_obj == self and p_list:
            p_list.reverse()
        return p_list
    
    def get_separator(self):
        return ' :: '
    
    def _parents_repr(self):
        p_list = self._recurse_for_parents(self)
        return self.get_separator().join(p_list)
    _parents_repr.short_description = 'Department parents'
    
    def save(self):
        p_list = self._recurse_for_parents(self)
        if self.department_title in p_list:
            raise exceptions.ValidationError('You must not save a category in itself')
        super(Department, self).save()

class Staffer(models.Model):
    def staff_photo_file_name(instance, filename):
        (orig_name, orig_ext) = path.splitext(filename)
        return 'uploads/staff/staff.%s.%s%s' % (instance.first_name_middle.lower(), instance.last_name.lower(), orig_ext)
    
    old_id = models.IntegerField(blank=True, null=True)
    old_site_id = models.IntegerField(blank=True, null=True)
    old_weblog_id = models.IntegerField(blank=True, null=True)
    cgi_id = models.IntegerField(blank=True, null=True)
    cgi_photo_id = models.IntegerField(blank=True, null=True)
    supra = models.BooleanField(blank=True, help_text=u'Use this check box if this staffer is to be listed at the top of the staff page.', verbose_name=u'In a top group?')
    supra_order = models.IntegerField(blank=True, null=True, help_text=u'Order of name within group at top of staff page. i.e, No. 5 in \'General news coverage\' section.', verbose_name=u'Order within group')
    twitter_id = models.CharField(u'Twitter handle', max_length=100, blank=True, help_text=u'Without the "@" sign.')
    photo = ImageField(upload_to=staff_photo_file_name, blank=True)
    first_name_middle = models.CharField(max_length=256, blank=True)
    last_name = models.CharField(max_length=256, blank=True)
    position = models.CharField(max_length=128, blank=True)
    bio = models.TextField(blank=True)
    contact_info = models.TextField(max_length=256, blank=True)
    address = models.TextField(max_length=300, blank=True)
    email = models.EmailField(blank=True)
    other_email = models.EmailField(blank=True)
    phone = models.CharField(max_length=26, blank=True)
    extension = models.CharField(max_length=26, blank=True)
    cell_phone = models.CharField(max_length=26, blank=True)
    home_phone = models.CharField(max_length=26, blank=True)
    more_info = models.CharField(max_length=256, blank=True)
    active = models.BooleanField(default=True)
    hire_date = models.DateField(null=True, blank=True)
    birth_date = models.DateField(blank=True, null=True)
    mystery_date_time = models.DateTimeField(blank=True, null=True, help_text=u'This datetime came from the data from the old cgi-bin system. Don\'t know what it means, but it was brought over.')
    last_modified = models.DateTimeField(auto_now=True)
    department = models.ForeignKey(Department, null=True)
    top_section = models.ForeignKey(TopSection, blank=True, null=True, verbose_name=u'Top section group name')
    
    def __unicode__(self):
        return u'%s %s' % (self.first_name_middle, self.last_name)
    
    def admin_thumbnail(self):
        if self.photo:
            im = get_thumbnail(self.photo, '60')
            return u'<a href="http://%s.%s/%s"><img src="%s" width="60" alt="%s %s" /></a>' % (self.photo.storage.bucket, self.photo.storage.connection.server, self.photo.name, im.url, self.first_name_middle, self.last_name)
        else:
            return u'(No photo)'
    admin_thumbnail.short_description = u'Thumbnail'
    admin_thumbnail.allow_tags = True
    
    def full_name(self):
        return u'%s %s' % (self.first_name_middle, self.last_name)
