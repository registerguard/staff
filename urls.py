from django.conf.urls.defaults import *
from django.views.generic import ListView
from django.views.generic.base import RedirectView
from staff.models import Staffer
from staff.views import StaffListView, PrivateStaffListView

urlpatterns = patterns('',
    (r'^phone-list/$', PrivateStaffListView.as_view(
        queryset = Staffer.objects.filter(active=True).exclude(extension='').order_by('last_name'),
    )),
    (r'^internal/$', ListView.as_view(
        queryset = Staffer.objects.filter(active=True).order_by('last_name'),
    )),
    (r'^$', RedirectView.as_view(url='http://www.registerguard.com/contact', permanent=True)),
)
