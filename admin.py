from django.contrib import admin
from sorl.thumbnail.admin import AdminImageMixin
from staff.models import Staffer, Department, TopSection

class StafferAdmin(AdminImageMixin, admin.ModelAdmin):
    list_display = ('last_name', 'first_name_middle', 'active', 'position', 'more_info', 'cell_phone', 'phone', 'twitter_id', 'supra', 'supra_order', 'top_section', 'email',)
    list_editable = ( 'active', 'cell_phone', 'twitter_id', 'position', 'more_info', 'phone', 'supra', 'supra_order', 'top_section',)
    list_filter = ('active', 'supra', 'department',)
    search_fields = ('last_name', 'first_name_middle',)
    ordering = ('last_name',)

admin.site.register(Staffer, StafferAdmin)

class DepartmentAdmin(admin.ModelAdmin):
    ordering = ('department_title',)
    prepopulated_fields = {'slug': ('department_title',)}

admin.site.register(Department, DepartmentAdmin)

class TopSectionAdmin(admin.ModelAdmin):
    ordering = ('top_section_group',)
    prepopulated_fields = {'slug': ('top_section_group',)}

admin.site.register(TopSection, TopSectionAdmin)
